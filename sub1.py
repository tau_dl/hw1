import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable

EPOCHS = 5000


class XorNet(nn.Module):
    def __init__(self):
        super(XorNet, self).__init__()
        self.fc1 = nn.Linear(2, 3)
        self.fc2 = nn.Linear(3, 1)

    def forward(self, x):
        x = torch.tanh(self.fc1(x))
        x = self.fc2(x)
        return x


def main():
    # creating training data
    x_train = map(lambda x: Variable(torch.Tensor(x)), [[0, 0], [0, 1], [1, 0], [1, 1]])
    y_train = map(lambda x: Variable(torch.Tensor(x)), [[0], [1], [1], [0]])
    dataset = list(zip(x_train, y_train))
    # Define the neural network, criterion, optimizer
    net = XorNet()
    criterion = nn.MSELoss()
    optimizer = optim.Adam(net.parameters())
    # Train the network
    for i in range(EPOCHS):
        for x, y in dataset:
            optimizer.zero_grad()
            y_model = net(x)
            loss = criterion(y_model, y)
            loss.backward()
            optimizer.step()
        if i % 100 == 0:
            print("Epoch: {}, Loss: {}".format(i, loss))
    # Test
    with open("results.txt", "w") as f:
        for x, _ in dataset:
            f.write("input: {}, output: {}\n".format(x.data, net(x).data))


if __name__ == '__main__':
    main()

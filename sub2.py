import torch
import numpy as np
import pandas as pd
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from torch.autograd import Variable

EPOCHS = 5000
DATASET = r"dataset\iris.data.csv"


def ReQU(x):
    indicator = (x > 0).type_as(x)
    return indicator.mul(x).mul(x)


class IrisNet(nn.Module):
    def __init__(self):
        super(IrisNet, self).__init__()
        self.fc1 = nn.Linear(4, 4)
        self.fc2 = nn.Linear(4, 3)
        self.log_soft_max = nn.LogSoftmax(dim=0)

    def forward(self, x):
        x = ReQU(self.fc1(x))
        x = self.log_soft_max(self.fc2(x))
        return x


def load_dataset(file_path):
    """
    :param str file_path: file path
    :return: Tuple of inputs and labels
    :rtype: (torch.Tensor, torch.Tensor)
    """
    iris_df = pd.read_csv(file_path, header=None)
    unique_labels = set(iris_df.iloc[:, -1])
    iris_spices = dict(zip(unique_labels, range(len(unique_labels))))
    iris_df.columns = ['SepalLength', 'SepalWidth', 'PetalLength', 'PetalWidth', 'Spice']
    x_train = torch.Tensor(iris_df[['SepalLength', 'SepalWidth', 'PetalLength', 'PetalWidth']].values)
    y_train = torch.Tensor(iris_df['Spice'].map(lambda spice: iris_spices[spice]).values)
    return x_train, y_train


def compute_accuracy(x_train, y_train, net):
    y_model = np.argmax(net(x_train).data, axis=1)
    count_equal = (y_model == y_train).sum().item()
    return count_equal / y_train.shape[0]


def main():
    x_train, y_train = load_dataset(DATASET)
    # Define the neural network, criterion, optimizer
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    net = IrisNet().to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters())
    targets = Variable(y_train).long().to(device)
    inputs = Variable(x_train).to(device)
    # Train the network
    all_losses = []
    for i in range(EPOCHS):
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()
        cur_loss = torch.mean(loss).item()
        all_losses.append(cur_loss)
        if i % 100 == 0:
            print('Epoch: {}, Loss: {}'.format(i, cur_loss))
    # Test
    print("Accuracy is : {}".format(compute_accuracy(inputs, targets.cpu(), net)))
    # Visualizing the loss over iterations
    all_losses = np.array(all_losses, dtype=np.float)
    plt.plot(all_losses)
    plt.title('convergence')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.show()


if __name__ == '__main__':
    main()

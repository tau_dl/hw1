import torch
import sklearn
import torch.utils.data
import numpy as np
import pandas as pd
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from torch.autograd import Variable
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

EPOCHS = 200
BATCH_SIZE = 10
LR = 1e-4
TRAINING_FILE = r"dataset\training.csv"


class View(nn.Module):
    def __init__(self, args):
        super(View, self).__init__()
        self.shape = args

    def forward(self, x):
        return x.view(-1, self.shape)


def load(file_path):
    """
    :param str file_path: file path
    :return: Tuple of inputs and labels
    :rtype: (torch.Tensor, torch.Tensor)
    """
    df = pd.read_csv(file_path)
    df = df.dropna()
    x = torch.Tensor(np.vstack(df['Image'].apply(lambda x: np.fromstring(x, sep=' '))))
    y = torch.Tensor(df[df.columns[:-1]].values)
    # data normalization
    x = x / 255
    y = (y - 48) / 48
    return x, y


def main():
    x, y = load(TRAINING_FILE)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    # ------------------------------------------- First Model -------------------------------------------------
    # Define the neural network, criterion, optimizer
    first_model = nn.Sequential(
        nn.Linear(9216, 100),
        nn.ReLU(),
        nn.Linear(100, 30),
    ).to(device)
    criterion = nn.MSELoss()
    optimizer = optim.Adam(first_model.parameters(), lr=LR)
    # split test and train
    inputs_train, inputs_test, labels_train, labels_test = train_test_split(x.numpy(), y.numpy(), shuffle=True,
                                                                            random_state=42, test_size=0.2)
    inputs_test_v, labels_test_v = map(lambda arr: torch.Tensor(arr).to(device), (inputs_test, labels_test))
    # Train the network
    all_train_losses = []
    all_test_losses = []
    for i in range(EPOCHS):
        inputs_train_v, labels_train_v = map(lambda arr: Variable(torch.Tensor(arr)).to(device),
                                             sklearn.utils.shuffle(inputs_train, labels_train, random_state=42))
        train_loss = criterion(first_model(inputs_train_v), labels_train_v)
        test_loss = criterion(first_model(inputs_test_v), labels_test_v)
        all_train_losses.append(train_loss.item())
        all_test_losses.append(test_loss.item())
        optimizer.zero_grad()
        train_loss.backward()
        optimizer.step()
        if i % 100 == 0:
            print('Epoch: {}, Loss: {}'.format(i, train_loss))
    # Report loss
    print("Loss Report:")
    print("train_loss : {}, test_loss : {}".format(train_loss.item(), test_loss.item()))
    # ------------------------------------------- Second Model -------------------------------------------------
    second_model = nn.Sequential(
        nn.Conv2d(1, 32, kernel_size=3),
        nn.MaxPool2d(2, stride=2),
        nn.ReLU(),
        nn.Conv2d(32, 64, kernel_size=2),
        nn.MaxPool2d(2, stride=2),
        nn.ReLU(),
        nn.Conv2d(64, 128, kernel_size=2),
        nn.MaxPool2d(2, stride=2),
        nn.ReLU(),
        View(15488),
        nn.Linear(15488, 500),
        nn.ReLU(),
        nn.Linear(500, 500),
        nn.ReLU(),
        nn.Linear(500, 30)
    ).to(device)
    criterion = nn.MSELoss()
    optimizer = optim.Adam(second_model.parameters())
    x_shaped = x.view((x.shape[0], 1, 96, 96))
    inputs_train, inputs_test, labels_train, labels_test = map(lambda arr: Variable(torch.Tensor(arr)).to(device),
                                                               train_test_split(x_shaped.numpy(), y.numpy(), shuffle=True, test_size=0.2, random_state=42))
    dataset_train = torch.utils.data.TensorDataset(inputs_train, labels_train)
    dataset_test = torch.utils.data.TensorDataset(inputs_test, labels_test)
    # Train the network
    all_train_losses2 = []
    all_test_losses2 = []
    for i in range(EPOCHS):
        # going over the train set
        total_train_batch_loss = 0
        for inputs_train_batch, labels_train_batch in torch.utils.data.DataLoader(dataset_train, shuffle=True, batch_size=BATCH_SIZE):
            optimizer.zero_grad()
            train_loss = criterion(second_model(inputs_train_batch), labels_train_batch)
            total_train_batch_loss += train_loss.item() * len(inputs_train_batch)
            train_loss.backward()
            optimizer.step()
        all_train_losses2.append(total_train_batch_loss / len(dataset_train))
        # going over the test set
        test_batch_loss = 0
        for inputs_test_batch, labels_test_batch in torch.utils.data.DataLoader(dataset_test, batch_size=BATCH_SIZE):
            test_loss = criterion(second_model(inputs_test_batch), labels_test_batch)
            test_batch_loss += test_loss.item() * len(inputs_test_batch)
        all_test_losses2.append(test_batch_loss / len(dataset_test))
        if i % 10 == 0:
            print('Epoch: {}, Train Loss: {}, Test Loss'.format(i, all_test_losses2[-1], all_test_losses2[-1]))

    # Report loss
    print("Loss Report:")
    print("first model train_loss : {}, first model test_loss : {}".format(all_train_losses[-1], all_test_losses[-1]))
    print("second model train_loss : {}, second model test_loss : {}".format(all_train_losses2[-1], all_test_losses2[-1]))
    # Visualizing the loss over iterations
    plt.subplot(2, 2, 1)
    plt.plot(all_train_losses2, label='Train loss')
    plt.plot(all_test_losses2, label='Test loss')
    plt.ylabel('MSE')
    plt.xlabel('Epoch')
    plt.title('complex model', fontsize=18)
    plt.legend()

    plt.subplot(2, 2, 2)
    plt.plot(all_train_losses, label='Train loss')
    plt.plot(all_test_losses, label='Test loss')
    plt.ylabel('MSE')
    plt.xlabel('Epoch')
    plt.title('simple model', fontsize=18)
    plt.show()


if __name__ == '__main__':
    main()
